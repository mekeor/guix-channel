(define-module (user packages mu-beta)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages mail)
  #:use-module (guix build utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public mu-beta
  (package
    (inherit mu)
    (version "1.5.13")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/djcb/mu")
                     (commit "7034556ab4d7b0fc36eda93ab4b7a5370fdbea83")))
              (file-name (git-file-name "mu" version))
              (sha256
                (base32
                  "1fcs9mrwx117nybg4avdd2q4qybvqpckny2v9bzz21q6a8g9yxz1"))))

    ;; the plain git-repository takes more effort to build than the archive.
    ;; we'll need to run autoreconf ourselves:

    (native-inputs
      `(("autoconf" ,autoconf)
         ("automake" ,automake)
         ("libtool" ,libtool)
         ,@(package-native-inputs mu)))

    (arguments 
      (substitute-keyword-arguments (package-arguments mu)
        ((#:phases phases)
          `(modify-phases ,phases

             ;; do not try to run ./autogen.sh or other fancy stuff, just
             ;; autoreconf:
             (replace 'bootstrap
               (lambda _ (invoke "autoreconf" "-vfi")))

             ;; move 'patch-configure to after 'bootstrap:
             (delete 'patch-configure)
             (add-after 'bootstrap 'patch-configure-for-git
               ;; By default, elisp code goes to "share/emacs/site-lisp/mu4e",
               ;; so our Emacs package can't find it.  Setting "--with-lispdir"
               ;; configure flag doesn't help because "mu4e" will be added to
               ;; the lispdir anyway, so we have to modify "configure.ac".
               (lambda _
                 (substitute* "configure"
                   (("^ +lispdir=\"\\$\\{lispdir\\}/mu4e/\".*") "")
                   ;; Use latest Guile
                   (("guile-2.0") "guile-2.2"))
                 (substitute* '("guile/Makefile.in"
                                 "guile/mu/Makefile.in")
                   (("share/guile/site/2.0/") "share/guile/site/2.2/"))
                 #t))

             ;; *.c (C) was renamed to *.cc (C++):
             (replace 'patch-bin-sh-in-tests
               (lambda _
                 (substitute* '("guile/tests/test-mu-guile.cc"
                                 "mu/test-mu-cmd.cc"
                                 "mu/test-mu-cmd-cfind.cc"
                                 "mu/test-mu-query.cc")
                   (("/bin/sh") (which "sh")))
                 #t))))))))
