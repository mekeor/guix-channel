(define-module (user packages python-domain-connect)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public python-domain-connect
  (package
    (name "python-domain-connect")
    (version "0.0.9")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/Domain-Connect/domainconnect_python.git")
         (commit
          (string-append "v" version))))
       (sha256
        (base32 "1xji0svamw961c7zgs1453cw2b9w94mk5qrfvqyb592l6yhmmm62"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'only-single-number-versions
          (lambda _
            (substitute* '("setup.py" "requirements.txt")
              (("(.*)([0-9]+)\\.[0-9]+\\.[0-9]+(.*)" all begin number end)
               (string-append begin number end "\n"))))))))
    (propagated-inputs
     `(("python-cffi" ,python-cffi)
       ("python-cryptography" ,python-cryptography)
       ("python-dnspython" ,python-dnspython)
       ;;("python-enum34" ,python-enum34)
       ("python-future" ,python-future)
       ;;("python-ipaddress" ,python-ipaddress)
       ("python-publicsuffix" ,python-publicsuffix)
       ("python-publicsuffixlist" ,python-publicsuffixlist)
       ("python-pycparser" ,python-pycparser)
       ("python-six" ,python-six)))
    (home-page "https://github.com/Domain-Connect/domainconnect_python")
    (synopsis "A python client libary for the open standard \"Domain
Connect\".")
    (description "This package provides a Python client library for the open
standard \"Domain Connect\" which can be used to connect an IP-address to a
domain.")
    (license license:expat)))

(define-public python-domain-connect-dyndns
  (package
    (name "python-domain-connect-dyndns")
    (version "0.0.9")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/Domain-Connect/DomainConnectDDNS-Python.git")
         (commit
          (string-append "v" version))))
       (sha256
        (base32 "024wxhfifl14j8s973lg6ls6s80grf9sm417kd2rpy1a90p89dnk"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'only-single-number-versions
          (lambda _
            (substitute* '("setup.py" "requirements.txt")
              (("(.*)([0-9]+)\\.[0-9]+\\.[0-9]+(.*)" all begin number end)
               (string-append begin number end "\n"))))))))
    (propagated-inputs
     `(("python-certifi" ,python-certifi)
       ("python-cffi" ,python-cffi)
       ("python-chardet" ,python-chardet)
       ("python-cryptography" ,python-cryptography)
       ("python-dnspython" ,python-dnspython)
       ("python-domain-connect" ,python-domain-connect)
       ("python-future" ,python-future)
       ("python-idna" ,python-idna)
       ;;("python-ipaddress" ,python-ipaddress)
       ("python-publicsuffix" ,python-publicsuffix)
       ("python-pycparser" ,python-pycparser)
       ("python-requests" ,python-requests)
       ("python-six" ,python-six)
       ("python-urllib3" ,python-urllib3)
       ("python-validators" ,python-validators)))
    (home-page "https://github.com/Domain-Connect/DomainConnectDDNS-Python")
    (synopsis "A Python client for the open standard \"Domain Connect\".")
    (description "This package provides a client for the open standard
\"Domain Connect\" which can be used to connect an IP-address to a domain.")
    (license license:expat)))

(define-public python-publicsuffixlist
  (package
    (name "python-publicsuffixlist")
    (version "0.7.7")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/ko-zu/psl.git")
         (commit "bff8d6a87b6bd3f6894e9211a9ee3c995ccfdcfc")))
       (sha256
        (base32 "1nzvw6n702y1v1z5b62lv2rnlqjr3hjpal2750sg8s713fxvxlzz"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/ko-zu/psl")
    (synopsis "Public Suffix List parser implementation for Python")
    (description "This package provides a Public Suffix List parser
implementation for Python.")
    (license license:mpl2.0)))
